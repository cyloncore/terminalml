#include <QDir>
#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQuickWindow>

#include <TerminalML/initialise.h>

int main(int argc, char* argv[])
{
    // do this before constructing QGuiApplication so that the custom logging rules will override the setFilterRules below
    qputenv("QT_LOGGING_CONF", QDir::homePath().toLocal8Bit() + "/.config/QtProject/qtlogging.ini");
    QGuiApplication app(argc, argv);
    TerminalML::initialise();
    QQmlApplicationEngine engine(QUrl("qrc:///main.qml"));
    return app.exec();
}
