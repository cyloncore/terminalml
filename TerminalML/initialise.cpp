#include <QQmlEngine>
#include <QLoggingCategory>

#include "cursor.h"
#include "mono_text.h"
#include "object_destruct_item.h"
#include "process.h"
#include "screen.h"
#include "selection.h"
#include "terminal_screen.h"
#include "text.h"

#if (QT_VERSION < QT_VERSION_CHECK(5, 14, 0))
template<typename _T_>
void qmlRegisterAnonymousType(const char*, int) { qmlRegisterType<_T_>(); }
#endif

namespace TerminalML
{

static const struct {
    const char *type;
    int major, minor;
} qmldir [] = {
    { "Screen", 1, 0},
    { "Text", 1, 0},
    { "Cursor", 1, 0},
    { "Selection", 1, 0},
};

  void initialise()
  {
    QLoggingCategory::setFilterRules(QStringLiteral("yat.*.debug=false\nyat.*.info=false"));
    
    qmlRegisterType<TerminalScreen>("TerminalML", 1, 0, "TerminalScreen");
    qmlRegisterType<ObjectDestructItem>("TerminalML", 1, 0, "ObjectDestructItem");
    qmlRegisterType<MonoText>("TerminalML", 1, 0, "MonoText");
    qmlRegisterUncreatableType<Process>("TerminalML", 1, 0, "Process", "For enum.");
    qmlRegisterAnonymousType<Screen>("TerminalML", 1);
    qmlRegisterAnonymousType<Text>("TerminalML", 1);
    qmlRegisterAnonymousType<Cursor>("TerminalML", 1);
    qmlRegisterAnonymousType<Selection>("TerminalML", 1);

    const QString filesLocation = "qrc:/qml/TerminalML";
    for (int i = 0; i < int(sizeof(qmldir)/sizeof(qmldir[0])); i++)
        qmlRegisterType(QUrl(filesLocation + "/" + qmldir[i].type + ".qml"), "TerminalML", qmldir[i].major, qmldir[i].minor, qmldir[i].type);
  }

}
